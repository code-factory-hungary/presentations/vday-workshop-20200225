# Nodes

#### Get kubernetes nodes
```
kubectl get nodes # simple  
kubectl get nodes -o wide # more informations  
```

#### Describe nodes
```
kubectl describe node <nodename>  
```

#### Select nodes based on label
```
kubectl get nodes -l cluster=all # get all nodes with cluster=all label  
kubectl get nodes --selector=cluster=all # same command as above  
```

# Pods

#### Generate yaml for a single pod
```
kubectl run --generator=run-pod/v1 vday --image=nginx \
--requests=cpu=100m,memory=256Mi \
--port=80 --labels=vday=workshop \
--dry-run -o yaml  
```

#### Run a disposable interactive shell
```
kubectl run --generator=run-pod/v1 disposable --image=busybox:1.28 --rm -it -- /bin/sh  
```

#### Expose pod
```
kubectl expose pod vday --name=vday-svc --port=80 --target-port=80  
```

# Deployments

#### Edit deployment with vim and save change clause on rollout history
```
kubectl edit deployment vday-deployment --record  
```

#### Get deployment rollout history
```
kubectl rollout history deployment magicdeployment  
```

#### Create deployment with kubectl run (deprecated but useful)
```
kubectl run vday-deployment --image=nginx:latest --labels="my=label" --limits="cpu=100m" --replicas=3  
```

# Job

#### Create a job that runs sleep 10 (see that only changing --restart changes object created) (deprecated but useful)
```
kubectl run vday-job --restart=OnFailure --image=busybox --command=true -- sleep 10  
```

# CronJob

#### Create a cronjob that executes sleep 10 every minutes (deprecated but useful)
```
kubectl run vday-scheduled-job --schedule="* * * * *" --restart=Never --image=busybox --command=true -- sleep 10  
```

# Networking

#### Debug networking, ping a pod
```
kubectl run --generator=run-pod/v1 disposable --image=busybox:latest --rm -it -- ping vday-svc.default.svc.cluster.local
```

#### Debug networking, resolve name
```
kubectl run --generator=run-pod/v1 disposable --image=busybox:1.28 -- sleep 3600 # creates a pod that runs for 1 hour
```
```
kubectl exec disposable -it -- nslookup vday-svc.default.svc.cluster.local
```

# Logging

#### Get logs in follow mode from container inside pod starting from last 100 lines
```
kubectl logs -f vday --container nginx --tail=100  
```

